# common.py
# cme_utils -> hoomd
jupyter
signac
matplotlib
pandas
scipy
gsd
# notebook diff
nbdime
git+https://github.com/mikemhenry/piecewise.git
git+https://bitbucket@bitbucket.org/cmelab/cme_utils.git
