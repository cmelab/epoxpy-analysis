## import os

import math

import gsd
import gsd.fl
import gsd.hoomd
import os
import numpy as np
import signac
import pandas as pd
from scipy.signal import argrelextrema as argex
import itertools

def savefig(plt,nbname,figname,transparent=True):
    import os
    if not os.path.exists(nbname):
        os.makedirs(nbname)
    plt.savefig(os.path.join(nbname,figname),transparent=transparent)
    
def get_all_maximas(Lx,q,intensities):
    half_box_length = Lx*0.6
    q_half_length = 2*math.pi/(half_box_length)
    peaks_q = []
    peaks_I = []
 #   print(q)
    maxima_i = argex(intensities,np.greater)[0]
    for i in maxima_i:
        if q[i] > q_half_length:
  #          print(i)
            peaks_q.append(q[i])
            peaks_I.append(intensities[i])
    #if len(peaks_I)==0:
    #    print(job)
    #    print(q_half_length,maxima_i)
    return peaks_q,peaks_I

def get_highest_maxima(Lx,q,intensities):
    peaks_q,peaks_I = get_all_maximas(Lx,q,intensities)
    if len(peaks_I) > 0:
        largest_peak_I = np.max(peaks_I)
        index_largest_I = peaks_I.index(largest_peak_I)
        largest_peak_q = peaks_q[index_largest_I]
    else:
        largest_peak_q=None
        largest_peak_I=None
    return largest_peak_q,largest_peak_I

def get_nth_maxima(job,q,intensities,n=1):
    '''
    Use 'n'=1 for first maxima and 'n'=2 for second maxima etc..
    '''
    peaks_q,peaks_I = get_all_maximas(job,q,intensities)
    sorted_peaks_I = np.sort(peaks_I)
    #print(peaks_q,peaks_I,sorted_peaks_I)
    nth_largest_peak_I = sorted_peaks_I[n*-1]
    index_nth_largest_I = peaks_I.index(nth_largest_peak_I)
    nth_largest_peak_q = peaks_q[index_nth_largest_I]
    return nth_largest_peak_q,nth_largest_peak_I

def save_frame(job,frame):
    with gsd.hoomd.open('Frame{}.gsd'.format(frame), 'wb') as t_new:
        f = gsd.fl.GSDFile(job.fn('data.gsd'), 'rb')
        t = gsd.hoomd.HOOMDTrajectory(f)
        snap = t[frame]
        t_new.append(snap)
    #hoomd.deprecated.dump.xml(group=hoomd.group.all(), filename=job.fn('Frame{}.hoomdxml'.format(frame)), position=True)

from mpl_toolkits.mplot3d import axes3d

class MyAxes3D(axes3d.Axes3D):

    def __init__(self, baseObject, sides_to_draw):
        self.__class__ = type(baseObject.__class__.__name__,
                              (self.__class__, baseObject.__class__),
                              {})
        self.__dict__ = baseObject.__dict__
        self.sides_to_draw = list(sides_to_draw)
        self.mouse_init()

    def set_some_features_visibility(self, visible):
        for t in self.w_zaxis.get_ticklines() + self.w_zaxis.get_ticklabels():
            t.set_visible(visible)
        self.w_zaxis.line.set_visible(visible)
        self.w_zaxis.pane.set_visible(visible)
        self.w_zaxis.label.set_visible(visible)

    def draw(self, renderer):
        # set visibility of some features False 
        self.set_some_features_visibility(False)
        # draw the axes
        super(MyAxes3D, self).draw(renderer)
        # set visibility of some features True. 
        # This could be adapted to set your features to desired visibility, 
        # e.g. storing the previous values and restoring the values
        self.set_some_features_visibility(True)

        zaxis = self.zaxis
        draw_grid_old = zaxis.axes._draw_grid
        # disable draw grid
        zaxis.axes._draw_grid = False

        tmp_planes = zaxis._PLANES

        if 'l' in self.sides_to_draw :
            # draw zaxis on the left side
            zaxis._PLANES = (tmp_planes[2], tmp_planes[3],
                             tmp_planes[0], tmp_planes[1],
                             tmp_planes[4], tmp_planes[5])
            zaxis.draw(renderer)
        if 'r' in self.sides_to_draw :
            # draw zaxis on the right side
            zaxis._PLANES = (tmp_planes[3], tmp_planes[2], 
                             tmp_planes[1], tmp_planes[0], 
                             tmp_planes[4], tmp_planes[5])
            zaxis.draw(renderer)

        zaxis._PLANES = tmp_planes

        # disable draw grid
        zaxis.axes._draw_grid = draw_grid_old
        
from scipy.optimize import curve_fit

def f_t(times,tau,A0,q_zero):
    return q_zero+(A0*np.exp(-times/tau))

def get_tau(times,qs):
    f = lambda x, *p: f_t(x,p[0],p[1],p[2])
    popt, pcov = curve_fit(f,
                           times,
                           qs,
                           p0=[np.mean(times),1,0.1])
    tau = popt[0]
    A0=popt[1]
    q0=popt[2]
    fit_ydata = f_t(times,*popt)
    residuals = qs - fit_ydata
    ss_res = np.sum(residuals**2)
    ss_tot = np.sum((qs-np.mean(qs))**2)
    #print('ss_res',ss_res,'ss_tot',ss_tot)
    r_squared = 1 - (ss_res / ss_tot)
    return tau,A0,q0,r_squared

def wlf(T,tau_s,Ts,C1=8.86,C2=101.6):
    return tau_s*np.exp((-np.log(10)*C1*(T-Ts)/(C2+(T-Ts))))

def fit_wlf(kTs,taus,Ts_fixed=None):
    if Ts_fixed==None:
        f = lambda x, tau_s,Ts: wlf(x,tau_s,Ts)
        mul_fact=1
        taus_scaled=taus*mul_fact
        popt, pcov = curve_fit(f,
                               kTs,
                               taus_scaled,
                               p0=[np.mean(taus),np.mean(kTs)])
        tau_s = popt[0]
        Ts = popt[1]
    else:
        Ts=Ts_fixed
        f = lambda x, tau_s: wlf(x,tau_s,Ts)
        mul_fact=1
        taus_scaled=taus*mul_fact
        popt, pcov = curve_fit(f,
                               kTs,
                               taus_scaled,
                               p0=[np.mean(taus)])
        tau_s = popt[0]
    #p=[1,10]
   
    
    fit_ydata = wlf(kTs,tau_s,Ts)/mul_fact
    residuals = taus - fit_ydata
    ss_res = np.sum(residuals**2)
    ss_tot = np.sum((taus-np.mean(taus))**2)
    #print('ss_res',ss_res,'ss_tot',ss_tot)
    r_squared = 1 - (ss_res / ss_tot)
    return tau_s,Ts,r_squared

def get_qm(job,qs,Is):
    first_peak_q,first_peak_i = get_highest_maxima(job.document['Lx'],qs,Is)
    return first_peak_q,first_peak_i
                        
    
                    
def get_qms(project,df_trial,trial,q_cut=1.0,typeId=2,n_views=40,grid_size=512):
    times = []
    qms = []
    for signac_id in df_trial.index:
        job = project.open_job(id=signac_id)
        if 'Lx' in job.document and job.isfile('qm.txt'):
            half_box_length = job.document['Lx']/2
            #print('--===found qm in ',kT,job)
        else:
            #print('Lx not found in',job)
            break
        #if ((job.sp.kT==3.0 and job.sp.trial>=3)|
        #   (job.sp.kT==2.8 and job.sp.trial>=3)):
        #    break

        diffract_dir_pattern ='diffract_type_{}_n_views_{}_grid_size_{}_frame'.format(typeId,
                                                                                     n_views,
                                                                                     grid_size)
        directories = os.listdir(job.workspace())
        directories = [d for d in os.listdir(job.workspace()) if d.startswith(diffract_dir_pattern)]
        directories.sort(key = lambda x: int(x.split('_')[-1]))
        num_frames = len(directories)
        for i,diffract_dir in enumerate(directories):
            if diffract_dir.startswith(diffract_dir_pattern):
                frame = int(diffract_dir.split('_')[-1])
                if frame%1==0:# and frame >9.9e6/job.sp.dcd_write:# and frame <5.5e6/job.sp.dcd_write:#==119 or frame==123:#%100 == 0:#num_frames/30:
                    if job.isfile('{}/asq.txt'.format(diffract_dir)):
                        data=np.genfromtxt(job.fn('{}/asq.txt'.format(diffract_dir)))
                        time = round(frame*job.sp.dcd_write)
                        qs = data[:,0]
                        Is = data[:,1]
                        Is=Is[qs<=q_cut]
                        qs=qs[qs<=q_cut]
                        qm,qmi = get_qm(job,qs,Is)
                        if qm!=None:
                            times.append(time)
                            qms.append(qm)
    #print(job.sp.md_dt)
    return np.asarray(times),np.asarray(qms)