import os
from multiprocessing import Pool

import numpy as np
import pandas as pd
import signac
from scipy.optimize import curve_fit


def func_autocat(times, a_start, H_1, H_2, m, n_1, Ea, kT):
    alphas = []
    alpha = a_start / 100
    for t in times:
        penalization = abs(2.5 - (m + n_1)) * 10
        k_1 = H_1 * np.exp(-Ea / (kT))
        k_2 = H_2 * np.exp(-Ea / (kT))
        dadt = (k_1 + k_2 * alpha ** m) * (1 - alpha) ** n_1
        alpha += dadt
        alphas.append(alpha * 100 * penalization)
    return alphas


def func_diffuse(times, a_start, a_max, H, n, Ea, kT):
    alphas = []
    alpha = a_start / 100
    for t in times:
        k = H * np.exp(-Ea / (kT))
        dadt = k * ((a_max - alpha) ** n)
        alpha += dadt
        alphas.append(alpha * 100)
    return alphas


def calc_r_squared(y_data, fit_data):
    residuals = y_data - fit_data
    ss_res = np.sum(residuals ** 2)
    ss_tot = np.sum((y_data - np.mean(y_data)) ** 2)
    if ss_tot == 0:
        r_squared = 0  # 1 - ss_res
    else:
        r_squared = 1 - (ss_res / ss_tot)
    return r_squared


def process_job(job):
    if "r_squared_both" in job.doc :
        return "skipping {}".format(job._id)
    try:
        df = pd.read_csv(job.fn("out.log"), sep="\t")
    except FileNotFoundError:
        return "no file with {}".format(job._id)
    bond_percents = df["bond_percent(A-B)"]
    N = len(bond_percents)
    if N < 3:
        # less than 3 data points, skipping
        return "less than 3 data points in {}".format(job._id)
    # cut = bond_percents.lt(90)
    time_steps = df["#timestep"]  # [cut]
    bond_percents = bond_percents  # [cut]

    Ea = job.sp.activation_energy
    kT = job.sp.kT

    las_n = -1 * np.argmax(np.gradient(np.gradient(bond_percents)))
    picewise = True
    if abs(las_n) < 1:
        picewise = False

    a_start = bond_percents[0]
    a_max = bond_percents.iloc[-1] / 100

    if picewise:
        time_steps_cut_post = time_steps[las_n:]
        bond_percents_cut_post = bond_percents[las_n:]
        a_start_post = bond_percents_cut_post.iloc[0]
        a_max_post = bond_percents_cut_post.iloc[-1] / 100

        time_steps_cut_pre = time_steps[:las_n]
        bond_percents_cut_pre = bond_percents[:las_n]
        a_start_pre = bond_percents_cut_pre.iloc[0]
    try:
        popt, pcov = curve_fit(
            lambda times, H_1, H_2, m, n_1: func_autocat(
                times, a_start, H_1, H_2, m, n_1, Ea, kT
            ),
            time_steps,
            bond_percents,
            bounds=[0, np.inf],
        )
        popt_diff, pcov_diff = curve_fit(
            lambda times, H, n: func_diffuse(times, a_start, a_max, H, n, Ea, kT),
            time_steps,
            bond_percents,
            bounds=[0, np.inf],
            maxfev=(10000 * (N + 1)),
        )
        if picewise:
            popt_post, pcov_post = curve_fit(
                lambda times, H, n: func_diffuse(
                    times, a_start_post, a_max_post, H, n, Ea, kT
                ),
                time_steps_cut_post,
                bond_percents_cut_post,
                bounds=[0, np.inf],
                maxfev=(10000 * (N + 1)),
            )
            popt_pre, pcov_pre = curve_fit(
                lambda times, H_1, H_2, m, n_1: func_autocat(
                    times, a_start_pre, H_1, H_2, m, n_1, Ea, kT
                ),
                time_steps_cut_pre,
                bond_percents_cut_pre,
                bounds=[0, np.inf],
            )
    except (RuntimeError, ValueError) as e:
        return "problem {} {} {} {}".format(e, N, las_n, job._id)
    fit_data = func_autocat(time_steps, a_start, *popt, Ea, kT)
    fit_data_diff = func_diffuse(time_steps, a_start, a_max, *popt_diff, Ea, kT)
    r_squared = calc_r_squared(bond_percents, fit_data)
    r_squared_diff = calc_r_squared(bond_percents, fit_data_diff)
    if picewise:
        fit_data_post = func_diffuse(
            time_steps_cut_post, a_start_post, a_max_post, *popt_post, Ea, kT
        )
        fit_data_pre = func_autocat(time_steps_cut_pre, a_start_pre, *popt_pre, Ea, kT)
        r_squared_post = calc_r_squared(bond_percents_cut_post, fit_data_post)
        r_squared_pre = calc_r_squared(bond_percents_cut_pre, fit_data_pre)
        pre_weight, post_weight = len(fit_data_pre) / N, len(fit_data_post) / N
        r_squared_both = r_squared_pre * pre_weight + r_squared_post * post_weight
        job.doc.r_squared_both = r_squared_both
    job.doc.r_squared_auto = r_squared
    job.doc.r_squared_diff = r_squared_diff
    return job._id


if __name__ == "__main__":
    # project = signac.get_project()
    project = signac.get_project("/home/mikehenry/epoxy-stuff/new_chem_data")
    jobs = project.find_jobs(doc_filter={"cure_percent": {"$gte": 89}})
    pool = Pool(processes=os.cpu_count() - 1)
    for my_id in pool.imap_unordered(process_job, jobs):
        print(my_id)
    pool.close()
