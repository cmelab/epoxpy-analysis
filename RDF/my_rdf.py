import signac
import matplotlib.pyplot as plt
import freud
import gsd.hoomd

data_path = "/home/mikehenry/epoxy-stuff/doc_test_enth"

project = signac.get_project(data_path)
jobs = project.find_jobs(filter={"bond": True}, doc_filter={"cure_percent":{"$gte":80}})

for job in jobs:
    print(job)
    print(job.fn("restart.gsd"))
    try:
        with gsd.hoomd.open(job.fn("restart.gsd")) as trajectory:
            freud_rdf = freud.density.RDF(bins=100, r_max=5,)
            freud_rdf.compute(system=trajectory[-1])
            plt.plot(freud_rdf.bin_centers, freud_rdf.rdf)
            plt.show()
    except IndexError:
        continue
