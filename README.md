Most of this analysis as been cleaned up is available [here](https://bitbucket.org/cmelab/notebooks-for-tg-paper/src/dirty/)

Some important notebooks in this repository:

* [Parametrize CG-LJ parameters from atomistic MD](https://bitbucket.org/cmelab/epoxpy-analysis/src/4bece68a99a4bcb381e5b4df551f3f31955d6e3d/DGEBF%20Paramterization.ipynb?at=master&viewer=nbviewer)
* [Find best kinetic fitting parameters](https://bitbucket.org/cmelab/epoxpy-analysis/src/master/rnx_fitting_theroy/Untitled.ipynb?viewer=nbviewer)
* [Plot species concentration as a function of DOC](https://bitbucket.org/cmelab/epoxpy-analysis/src/master/DOC_RXN_TUNE/calc_tgddm.ipynb?viewer=nbviewer)
* [Plot of Volume as a function of qunech kT](TG Volume/tg-volume.ipynb)
